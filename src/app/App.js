import React, { Component, PropTypes } from 'react';
import 'whatwg-fetch';
import Layout from './layouts/Layout';

class App extends Component {

  static propTypes = {
    children: PropTypes.node
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Layout>
        {this.props.children}
      </Layout>
    );
  }

}

export default App;
