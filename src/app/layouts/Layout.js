import React, {PropTypes} from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';

import NavBar from './components/NavBar';
import Auth from '../modules/Auth';
import { loggedIn } from '../modules/Auth/auth';

injectTapEventPlugin();

class Layout extends React.Component {

  static propTypes = {
    children: PropTypes.node
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedIn: loggedIn()
    };
    this._toggleLoggedIn = this._toggleLoggedIn.bind(this);
  }

  _toggleLoggedIn() {
    this.setState({
      loggedIn: loggedIn()
    });
  }

  _onToTop() {
    window.scrollTo(0, 0);
  }

  _onToBottom() {
    window.scrollTo(0, document.body.scrollHeight);
  }

  render() {
    return (
      <div>
        <NavBar loggedIn={this.state.loggedIn} />
        { this.state.loggedIn === true ? React.cloneElement(this.props.children, {toggleLoggedIn: this._toggleLoggedIn}) :
          <Auth toggleLoggedIn={this._toggleLoggedIn} />
        }
        <div className="container">
          <footer className="footer">
            <p className="text-muted">&copy; 2017 丨〇</p>
          </footer>
        </div>
        <input className="btn btn-default" type="button" value="T" style={{position: 'fixed', right: '30px', bottom: '63px'}} onClick={this._onToTop} />
        <input className="btn btn-default" type="button" value="B" style={{position: 'fixed', right: '30px', bottom: '30px'}} onClick={this._onToBottom} />
      </div>
    );
  }
}

export default Layout;
