import React, {PropTypes} from 'react';

export default class ArrayList extends React.Component {
  static propTypes = {
    data: PropTypes.array
  }

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data || {},
      nRow: 1,
      nCol: 1
    };
  }
}