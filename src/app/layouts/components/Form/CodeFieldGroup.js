import React, {PropTypes} from 'react';
import { FormGroup, ControlLabel, HelpBlock } from 'react-bootstrap/lib';
import CodeMirror from 'react-codemirror';

import htmlComposer from '../htmlComposer';

const FieldGroup = ({ id, label, help, mode, readOnly, value, ...props }) => {
  let finalValue = value;
  if (mode) {
    switch (mode) {
      case 'xml':
        finalValue = htmlComposer(value);
        break;
      default:
        finalValue = value;
        break;
    }
  }
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <CodeMirror {...props} value={finalValue} options={{mode: typeof mode !== 'undefined' ? mode : 'javascript', readOnly: typeof readOnly !== 'undefined' ? readOnly : true, lineNumbers: true, lineWrapping: true, tabSize: 2}} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
};

FieldGroup.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object]),
  id: PropTypes.string,
  label: PropTypes.string,
  help: PropTypes.string,
  mode: PropTypes.string,
  readOnly: PropTypes.bool
};

export default FieldGroup;
