import React, {PropTypes} from 'react';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap/lib';
import { PhotoshopPicker } from 'react-color';

import htmlComposer from '../htmlComposer';

export default class HtmlEditor extends React.Component {

  static propTypes = {
    getHtml: PropTypes.func,
    innerHtml: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object])
  };

  constructor(props) {
    super(props);
    this.state = {
      colorPickerShow: false,
      colorPickerValue: '',
      colorPickerTo: 'foreColor',
      foreColor: '#000',
      backColor: '#fff'
    };
    this.__initHtml = '<h1>&lt; Click to Edit &gt;</h1>';
    this._initHtml = this.__initHtml;
    // Choosed inline style before applied
    this._inlineStyle = {};
    this._getSelection = this._getSelection.bind(this);
    this._setBlock = this._setBlock.bind(this);
    this._setInlineStyle = this._setInlineStyle.bind(this);
    this._setForegroundColor = this._setForegroundColor.bind(this);
    this._setBackgroundColor = this._setBackgroundColor.bind(this);
    this.getHtml = this.getHtml.bind(this);
    this._onCloseColorPicker  = this._onCloseColorPicker.bind(this);
    this._onColorPickerChange = this._onColorPickerChange.bind(this);
    this._applyInlineStyle = this._applyInlineStyle.bind(this);
    this._resetHtml = this._resetHtml.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.innerHtml) {
      // Update will have init value
      this._initHtml = htmlComposer(nextProps.innerHtml);
      if (isNaN(this._initHtml.charCodeAt())) {
        this._initHtml = this.__initHtml;
      }
      if (this.props.getHtml) {
        this.props.getHtml(this._initHtml);
      }
    }
  }

  _getSelection() {
    const selectedText = window.getSelection().toString();
    console.log('Selection: [' + selectedText + ']');
    if (selectedText === '' || this.display.textContent.indexOf(selectedText) !== -1) {
      return selectedText;
    }
    return null;
  }

  _setBlock(e) {
    const selected = this._getSelection();
    if (selected) {
      document.designMode = 'on';
      document.execCommand('formatBlock', false, e.target.getAttribute('data-block-type'));
      document.designMode = 'off';
    }
    if (this.props.getHtml) {
      this.props.getHtml(this.display.innerHTML);
    }
  }

  _setInlineStyle(e) {
    const target = e.target;
    const styleType = target.getAttribute('data-inline-type');
    if (target.classList.contains('active')) {
      if (this._inlineStyle[styleType] !== null) {
        delete this._inlineStyle[styleType];
      }
    } else {
      this._inlineStyle[styleType] = target;
    }
    console.log(this._inlineStyle);
    target.classList.toggle('active');
  }

  _setForegroundColor() {
    this.setState({
      colorPickerValue: this.state.foreColor,
      colorPickerShow: true,
      colorPickerTo: 'foreColor'
    });
  }

  _setBackgroundColor() {
    this.setState({
      colorPickerValue: this.state.backColor,
      colorPickerShow: true,
      colorPickerTo: 'backColor'
    });
  }

  getHtml() {
    console.log(this.display.innerHTML);
    if (this.props.getHtml) {
      this.props.getHtml(this.display.innerHTML);
    }
  }

  _onCloseColorPicker() {
    this.setState({colorPickerShow: false});
  }

  _onColorPickerChange(color) {
    this.setState({
      colorPickerValue: color.hex,
      [this.state.colorPickerTo]: color.hex
    });
    this._inlineStyle[this.state.colorPickerTo] = color.hex;
  }

  _applyInlineStyle() {
    const selected = this._getSelection();
    if (selected) {
      let tagStyle = '';
      for (const attr in this._inlineStyle) {
        if (this._inlineStyle.hasOwnProperty(attr)) {
          if (typeof this._inlineStyle[attr] === 'object') {
            this._inlineStyle[attr].classList.remove('active');
          }
          switch (attr) {
            case 'b':
              tagStyle += ' font-weight: bold;';
              break;
            case 'i':
              tagStyle += ' font-style: italic;';
              break;
            case 'u':
              tagStyle += ' text-decoration: underline;';
              break;
            case 'foreColor':
              tagStyle += ' color: ' + this._inlineStyle.foreColor + ';';
              this.setState({foreColor: '#000'});
              break;
            case 'backColor':
              tagStyle += ' background-color: ' + this._inlineStyle.backColor + ';';
              this.setState({backColor: '#fff'});
              break;
            default:
              break;
          }
        }
      }

      const html = '<span style="' + tagStyle + '">' + selected + '</span>';
      document.designMode = 'on';
      console.log('formatInline:', document.execCommand('insertHTML', false, html));
      document.designMode = 'off';
      // clear the list
      this._inlineStyle = {};
    }
    if (this.props.getHtml) {
      this.props.getHtml(this.display.innerHTML);
    }
  }

  _resetHtml() {
    this.display.innerHTML = this._initHtml;
    if (this.props.getHtml) {
      this.props.getHtml(this._initHtml);
    }
  }

  render() {
    return (
      <div>
        <ButtonToolbar>
          <ButtonGroup>
            <Button data-block-type="h1" onClick={this._setBlock}>H1</Button>
            <Button data-block-type="h2" onClick={this._setBlock}>H2</Button>
            <Button data-block-type="h3" onClick={this._setBlock}>H3</Button>
            <Button data-block-type="h4" onClick={this._setBlock}>H4</Button>
            <Button data-block-type="p" onClick={this._setBlock}>P</Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button data-inline-type="b" onClick={this._setInlineStyle}><b className="no-events">B</b></Button>
            <Button data-inline-type="i" onClick={this._setInlineStyle}><i className="no-events">I</i></Button>
            <Button data-inline-type="u" onClick={this._setInlineStyle}><u className="no-events">U</u></Button>
            <Button data-inline-type="c"
                    onClick={this._setForegroundColor}
                    style={{color: this.state.foreColor, backgroundColor: this.state.backColor}}
            >Foreground Color</Button>
            <Button data-inline-type="c"
                    onClick={this._setBackgroundColor}
                    style={{color: this.state.foreColor, backgroundColor: this.state.backColor}}
            >Background Color</Button>
            <Button data-inline-type="apply" onClick={this._applyInlineStyle}>Apply</Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button data-block-type="h1" onClick={this.getHtml}>Output</Button>
            <Button onClick={this._resetHtml}>Reset</Button>
          </ButtonGroup>
        </ButtonToolbar>
        {
          this.state.colorPickerShow ?
          <div style={{position: 'absolute', zIndex: '2'}}>
            <div style={{position: 'fixed', top: '0px', right: '0px', bottom: '0px', left: '0px'}} onClick={this._onCloseColorPicker} />
            <PhotoshopPicker color={this.state.colorPickerValue} onChange={this._onColorPickerChange} />
          </div> : null
        }
        <div className="well well-lg text-center" ref={(display) => { this.display = display }} contentEditable={true} dangerouslySetInnerHTML={{__html: this._initHtml}} />
      </div>
    );
  }
}
