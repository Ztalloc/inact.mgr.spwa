import React, {PropTypes} from 'react';

import htmlComposer from '../htmlComposer';

export default class HtmlViewer extends React.Component {

  static propTypes = {
    html: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string])
  }

  constructor(props) {
    super(props);
    this.state = {
      html: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({html: htmlComposer(nextProps.html)});
  }

  render() {
    return (
      <div className="well well-lg text-center" dangerouslySetInnerHTML={{__html: this.state.html}} />
    );
  }
}
