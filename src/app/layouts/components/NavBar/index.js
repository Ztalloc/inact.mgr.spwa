import React, {PropTypes} from 'react';
import { Navbar, Nav, NavItem, Glyphicon, NavDropdown, MenuItem } from 'react-bootstrap/lib';
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap';
import { getUser } from '../../../modules/Auth/auth';

class NavBar extends React.Component {

  static propTypes = {
    loggedIn: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedItem: 0
    };
  }

  _select = (index) => {
    this.setState({
      selectedItem: index
    });
  }

  render() {
    return (
      <Navbar collapseOnSelect={true}>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">&#123; 实务鼠 &#125;</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <IndexLinkContainer to="/">
              <NavItem eventKey={1}>主页</NavItem>
            </IndexLinkContainer>
            <LinkContainer to="/user">
              <NavItem eventKey={2}>用户管理</NavItem>
            </LinkContainer>
            <LinkContainer to="/question">
              <NavItem eventKey={3}>问题管理</NavItem>
            </LinkContainer>
            <LinkContainer to="/survey">
              <NavItem eventKey={4}>问卷管理</NavItem>
            </LinkContainer>
            <LinkContainer to="/asset">
              <NavItem eventKey={5}>资源管理</NavItem>
            </LinkContainer>
          </Nav>
          <Nav pullRight={true}>
          {
            this.props.loggedIn &&
            <NavDropdown eventKey={6} id="dropdownUser" title={<span><Glyphicon glyph="user"/> {getUser().nickName}</span>}>
              <MenuItem eventKey={6.1}><Glyphicon glyph="certificate" /> Profile</MenuItem>
              <MenuItem eventKey={6.2}><Glyphicon glyph="cog" /> Management</MenuItem>
              <MenuItem divider />
              <LinkContainer to="/logout">
                <MenuItem eventKey={6.3}><Glyphicon glyph="remove-circle" /> Logout</MenuItem>
              </LinkContainer>
            </NavDropdown>
          }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }

}

export default NavBar;

