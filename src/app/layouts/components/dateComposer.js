module.exports = (strDateTime) => {
  if (!/\d{4}-\d{2}-\d{2}T\d{2}:.+/.test(strDateTime)) {
    return strDateTime;
  }
  const date = new Date(strDateTime);
  // At the browser's timezone
  const month = date.getMonth() > 8 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
  return date.getFullYear() + '-' + month + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
};
