const composeHtmlArray = (varArray, html) => {
  if (varArray.length !== 0) {
    varArray.forEach((value) => {
      if (typeof value[0] !== 'undefined') {
        composeHtmlArray(value, html);
      } else {
        composeHtmlObject(value, html);
      }
    });
  }
};

const composeHtmlObject = (varObject, html) => {
  for (const key in varObject) {
    if (varObject.hasOwnProperty(key)) {
      if (typeof varObject[key][0] === 'undefined') {
        html.push('<' + key);
        for (const attrName in varObject[key]) {
          if (attrName !== 'text') {
            html.push(' ' + attrName + '="' + varObject[key][attrName] + '"');
          }
        }
        html.push('>' + varObject[key].text + '</' + key + '>');
      } else {
        // array
        html.push('<' + key + '>');
        composeHtmlArray(varObject[key], html);
        html.push('</' + key + '>');
      }
    }
  }
};

const composeHtml = (nodes) => {
  const html = [''];
  if (nodes[0]) {
    // nodes is array
    composeHtmlArray(nodes, html);
  } else {
    // node is object
    composeHtmlObject(nodes, html);
  }

  return html.join('');
};

module.exports = (objHtml) => {
  if (typeof objHtml === 'object') {
    return composeHtml(objHtml);
  }
  return objHtml;
};
