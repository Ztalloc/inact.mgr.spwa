const restc = require('./restc');

module.exports = {
  uploadAsset: (type, fileFormData, callback) => {
    restc('build/upload_asset/_type/' + type, 'POST', fileFormData, (json) => {
      callback(json);
    }, false, true);
  }
};
