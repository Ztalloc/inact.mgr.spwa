const restc = require('./restc');

module.exports = {
  quantity: (callback) => {
    restc('question/quantity', 'GET', (json) => {
      callback(json);
    });
  },
  get: (callback) => {
    restc('question', 'GET', (json) => {
      callback(json);
    });
  },
  getOne: (_id, callback) => {
    restc('question/_/' + btoa(_id), 'GET', (json) => {
      callback(json);
    });
  },
  new: (data, callback) => {
    restc('question', 'POST', data, (json) => {
      callback(json);
    });
  },
  update: (_id, data, callback) => {
    const updateData = {$set: data};
    restc('question/_/' + btoa(_id), 'PUT', updateData, (json) => {
      callback(json);
    });
  },
  result: (questionId, callback) => {
    restc('question/result/_/' + btoa(questionId), 'GET', (json) => {
      callback(json);
    });
  },
  slist: (_sId, callback) => {
    restc('question/ls?_sId=' + _sId, 'GET', (json) => {
      callback(json);
    });
  }
};
