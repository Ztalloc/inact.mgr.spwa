const config = require('../../../config/datasource');
module.exports = (url, method, data, cb, resJson = true, isFile = false) => {
  const options = {
    method: method,
    headers: config.headers
  };
  let callback = cb;
  if (method !== 'GET' && method !== 'DELETE') {
    if (isFile) {
      const fileData = new FormData();
      fileData.append(data.fileName, data.file);
      options.body = fileData;
      if (options.headers['Content-Type']) {
        delete options.headers['Content-Type'];
      }
    } else {
      options.body = JSON.stringify(data, null, '');
      options.headers['Content-Type'] = 'application/json';
    }
  } else {
    if (typeof data === 'function') {
      callback = data;
    }
  }
  console.log('options', options);
  fetch(config.baseUrl + url, options).then((resp) => {
    if (resp.ok) {
      if (resJson) {
        return resp.json();
      }
      return resp.text();
    }
    return null;
  }).then((json) => {
    console.log(json);
    callback(json);
  });
};
