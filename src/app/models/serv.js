const restc = require('./restc');

module.exports = {
  info: (callback) => {
    restc('_status/info', 'GET', (json) => {
      callback(json);
    });
  }
};
