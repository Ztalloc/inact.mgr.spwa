const restc = require('./restc');

module.exports = {
  quantity: (callback) => {
    restc('survey/quantity', 'GET', (json) => {
      callback(json);
    });
  },
  get: (callback) => {
    restc('survey', 'GET', (json) => {
      callback(json);
    });
  },
  getOne: (_id, callback) => {
    restc('survey/_/' + btoa(_id), 'GET', (json) => {
      callback(json);
    });
  },
  new: (data, callback) => {
    restc('survey', 'POST', data, (json) => {
      callback(json);
    });
  },
  update: (_id, data, callback) => {
    const updateDate = {$set: data};
    restc('survey/_/' + btoa(_id), 'PUT', updateDate, (json) => {
      callback(json);
    });
  },
  result: (_id, callback) => {
    restc('survey/result/_/' + btoa(_id), 'GET', (json) => {
      callback(json);
    });
  },
  build: (surveyId, callback) => {
    restc('build/survey/' + btoa(surveyId), 'GET', null, (json) => {
      callback(json);
    }, false);
  },
  buildPreview: (surveyId, callback) => {
    restc('build/preview/_sI/' + btoa(surveyId), 'GET', null, (json) => {
      callback(json);
    }, false);
  },
  uploadAsset: (surveyName, fileFormData, callback) => {
    restc('build/upload_asset/_sN/' + btoa(surveyName), 'POST', fileFormData, (json) => {
      callback(json);
    }, false, true);
  }
};
