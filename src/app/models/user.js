const restc = require('./restc');

module.exports = {
  login: (email, password, callback) => {
    const loginData = {
      t: email,
      p: btoa(password)
    }
    restc('user/loginAdmin/_p/_', 'POST', loginData, (json) => {
      callback(json);
    });
  },
  get: (callback) => {
    restc('user/_p/_', 'GET', (json) => {
      callback(json);
    });
  },
  getOne: (_id, callback) => {
    // lastName firstName nickName email mobilePhone privilege status
    restc('user/_/' + btoa(_id) + '/_p/bGFzdE5hbWUgZmlyc3ROYW1lIG5pY2tOYW1lIGVtYWlsIG1vYmlsZVBob25lIHByaXZpbGVnZSBzdGF0dXM=', 'GET', (json) => {
      callback(json);
    });
  },
  update: (_id, data, callback) => {
    restc('user/' + btoa(_id), 'PUT', data, (json) => {
      callback(json);
    });
  },
  new: (data, callback) => {
    restc('user', 'POST', data, (json) => {
      callback(json);
    });
  }
};
