import React, {PropTypes} from 'react';
// import { browserHistory } from 'react-router';
import { Alert, PageHeader, Glyphicon, ButtonToolbar, Button, Panel } from 'react-bootstrap/lib';

// import { LinkContainer } from 'react-router-bootstrap';
import AssetData from '../../models/asset';

export default class Asset extends React.Component {

  static propTypes = {
    children: PropTypes.node
  }

  constructor() {
    super();
    this.state = {
      alert: false,
      alertMsg: '',
      btnLoading: false,
      btnText: '',
      audio: {},
      js: {},
      css: {}
    };
    this._onFileChange = this._onFileChange.bind(this);
    this._onUpload = this._onUpload.bind(this);
    this._onAlertDismiss = this._onAlertDismiss.bind(this);
  }

  _onFileChange(e) {
    e.preventDefault();
    this.setState({[e.target.getAttribute('data-type')]: e.target.files[0]});
    console.log( e.target.files[0], this.state.audio, e.target.getAttribute('data-type'));
  }

  _onUpload(e) {
    e.preventDefault();
    this.setState({btnLoading: true, btnText: 'Uploading [' + e.target.getAttribute('data-type') + '] asset...'})
    AssetData.uploadAsset(e.target.getAttribute('data-type'), {fileName: 'asset_file', file: this.state[e.target.getAttribute('data-type')]}, (txtResp) => {
      if (txtResp.match(/OK/)) {
        this.setState({alert: true, alertMsg: 'Succeed!'});
      }
      this.setState({btnLoading: false});
    });
  }

  _onAlertDismiss() {
    this.setState({alert: false});
  }

  render() {
    return (
      <div className="container">
        {
          this.state.alert &&
          <Alert bsStyle="info" onDismiss={this._onAlertDismiss}>
            <p>{this.state.alertMsg}</p>
          </Alert>
        }
        <PageHeader>Asset</PageHeader>
        <div className="row">
          <div className="col-md-12">
            <Panel header={<span><Glyphicon glyph="headphones" /> Audio</span>}>
              <form>
                <input type="file" data-type="audio" onChange={this._onFileChange} />
              </form>
              <ButtonToolbar style={{marginTop: '10px'}}>
                <Button bsStyle="primary" data-type="audio" onClick={!this.state.btnLoading ? this._onUpload : null} disabled={this.state.btnLoading}>{this.state.btnLoading ? this.state.btnText : 'Upload'}</Button>
              </ButtonToolbar>
            </Panel>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Panel header={<span><Glyphicon glyph="modal-window" /> Javascript</span>}>
              <form>
                <input type="file" data-type="js" onChange={this._onFileChange} />
              </form>
              <ButtonToolbar style={{marginTop: '10px'}}>
                <Button bsStyle="primary" data-type="js" onClick={!this.state.btnLoading ? this._onUpload : null} disabled={this.state.btnLoading}>{this.state.btnLoading ? this.state.btnText : 'Upload'}</Button>
              </ButtonToolbar>
            </Panel>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Panel header={<span><Glyphicon glyph="blackboard" /> CSS</span>}>
              <form>
                <input type="file" data-type="css" onChange={this._onFileChange} />
              </form>
              <ButtonToolbar style={{marginTop: '10px'}}>
                <Button bsStyle="primary" data-type="css" onClick={!this.state.btnLoading ? this._onUpload : null} disabled={this.state.btnLoading}>{this.state.btnLoading ? this.state.btnText : 'Upload'}</Button>
              </ButtonToolbar>
            </Panel>
          </div>
        </div>
        {
          this.props.children
        }
      </div>
    );
  }
}
