import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { logout } from './auth';

export default class Logout extends React.Component {

  static propTypes = {
    toggleLoggedIn: PropTypes.func
  }

  constructor() {
    super();
  }

  componentDidMount() {
    logout();
    browserHistory.push('/');
    this.props.toggleLoggedIn();
  }

  render() {
    return null;
  }
}
