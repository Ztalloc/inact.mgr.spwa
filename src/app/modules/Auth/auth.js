import User from '../../models/user';
const _requestAuth = (email, pass, cb) => {
  User.login(email, pass, (json) => {
    cb(json);
  });
};

function loggedIn() {
  if (localStorage.timestamp && (Date.now()) - localStorage.timestamp < 600000) {
    return !!localStorage.token;
  }
  delete localStorage.timestamp;
  delete localStorage.token;
  return false;
}

function update() {
  if (localStorage.timestamp && (Date.now()) - localStorage.timestamp < 600000) {
    localStorage.timestamp = Date.now();
  }
}
function login(user, pass, cb) {
  const callback = cb || arguments[arguments.length - 1];
  if (localStorage.token) {
    if (callback) callback(true);
    return;
  }
  _requestAuth(user, pass, (res) => {
    if (res.err.code === 0) {
      localStorage.token = res.data._id;
      localStorage.user = JSON.stringify(res.data);
      localStorage.timestamp = Date.now();
      if (callback) callback(true);
    } else {
      if (callback) callback(false);
    }
  });
}
function getUser() {
  if (localStorage.user) {
    return JSON.parse(localStorage.user);
  }
  return {};
}
function getToken() {
  return localStorage.token;
}
function logout(cb) {
  localStorage.clear();
  if (typeof cb === 'function') cb();
}

export { loggedIn, update, login, getUser, getToken, logout };
