import React, {PropTypes} from 'react';
import { Alert, PageHeader, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
const auth = require('./auth');

export default class Auth extends React.Component {

  static propTypes = {
    toggleLoggedIn: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedIn: auth.loggedIn(),
      alert: false,
      user: '',
      pass: ''
    };
    this._onInputChange = this._onInputChange.bind(this);
    this._onLogin = this._onLogin.bind(this);
    this._onAlertDismiss = this._onAlertDismiss.bind(this);
    this._onClearAll = this._onClearAll.bind(this);
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onLogin() {
    auth.login(this.state.user, this.state.pass, (res) => {
      if (res) {
        this.props.toggleLoggedIn();
      } else {
        this.setState({alert: true});
      }
    });
  }

  _onAlertDismiss() {
    this.setState({alert: false});
  }

  _onClearAll() {
    this.setState({
      user: '',
      pass: ''
    });
  }

  render() {
    return (
      <div className="container">
        {
          this.state.alert &&
          <Alert bsStyle="danger" onDismiss={this._onAlertDismiss}>
            <p>Username or password not valid!</p>
          </Alert>
        }
        <PageHeader>Auth</PageHeader>
        <div className="row">
          <div className="col-md-4 col-md-offset-4">
            <form autoComplete="off" onSubmit={(e) => {e.preventDefault()}}>
  	          <FieldGroup id="user" data-key="user" type="text" value={this.state.user} onChange={this._onInputChange} label="User" placeholder="Username/ID/Email" autoFocus={true} />
              <FieldGroup id="pass" data-key="pass" type="password" value={this.state.pass} onChange={this._onInputChange} label="Password" />
              <ButtonToolbar>
                <Button bsStyle="primary" type="submit" onClick={this._onLogin}>Login</Button>
                <Button bsStyle="link" onClick={this._onClearAll}>Clear All</Button>
              </ButtonToolbar>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
