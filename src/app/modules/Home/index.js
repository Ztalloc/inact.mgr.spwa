import React from 'react';
import { PageHeader } from 'react-bootstrap';

import ServData from '../../models/serv';
import SurveyData from '../../models/survey';
import QuestionData from '../../models/question';
import globalConfig from '../../../../config/global';

export default class Home extends React.Component {

  constructor() {
    super();
    this.state = {
      serv: {
        version: 'Loading...',
        protocol: 'Loading...'
      },
      survey: {
        quantity: 'Loading'
      },
      question: {
        quantity: 'Loading...'
      }
    }
  }

  componentWillMount() {
    ServData.info((json) => {
      this.setState({serv: json.data});
    });
    SurveyData.quantity((json) => {
      this.setState({survey: json.data});
    });
    QuestionData.quantity((json) => {
      this.setState({question: json.data});
    });
  }

  render() {
    return (
      <div className="container">
        <PageHeader>Info</PageHeader>
        <dl className="dl-horizontal">
          <dt>Client Version</dt><dd>{globalConfig.version}</dd>
          <dt>Serv Version</dt><dd>{this.state.serv.version}</dd>
          <dt>Serv Protocol</dt><dd>{this.state.serv.protocol}</dd>
          <dt>Survey Quantity</dt><dd>{this.state.survey.quantity}</dd>
          <dt>Question Quantity</dt><dd>{this.state.question.quantity}</dd>
        </dl>
      </div>
    );
  }
}
