import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import QuestionData from '../../models/question';

export default class New extends React.Component {

  static propTypes = {
    refresh: PropTypes.func,
    location: PropTypes.object
  }

  constructor() {
    super();
    this.state = {
      questionId: '',
      question: '',
      description: '',
      opts: '',
      settings: '',
      html: ''
    };
    this._sId = null;
    this._onInputChange = this._onInputChange.bind(this);
    this._onSubmit = this._onSubmit.bind(this);
    this._onClose = this._onClose.bind(this);
    this._onOptsChange = this._onOptsChange.bind(this);
    this._onSettingsChange = this._onSettingsChange.bind(this);
    this._onHtmlChange = this._onHtmlChange.bind(this);
    this._onClearAll = this._onClearAll.bind(this);
  }

  componentWillMount() {
    if (this.props.location.query._sId) {
      this._sId = this.props.location.query._sId;
    }
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onOptsChange(text) {
    this.setState({opts: text});
  }

  _onSettingsChange(text) {
    this.setState({settings: text});
  }

  _onHtmlChange(text) {
    this.setState({html: text});
  }

  _onSubmit(e) {
    e.preventDefault();
    const data = Object.assign({}, this.state);
    // array
    if (data.opts.trim() === '') {
      delete data.opts;
    } else {
      data.opts = JSON.parse(data.opts);
    }
    // object
    if (data.settings.trim() === '') {
      delete data.settings;
    } else {
      data.settings = JSON.parse(data.settings);
    }
    // string
    if (data.html.trim() === '') {
      delete data.html;
    }
    data.creatorId = 1;
    QuestionData.new(data, (json) => {
      if (json.err.code === 0) {
        this.props.refresh();
      }
    });
  }

  _onClearAll() {
    this.setState({
      questionId: '',
      question: '',
      decription: '',
      opts: '',
      settings: '',
      html: ''
    });
  }

  _onClose() {
    browserHistory.push('/question' + (this._sId ? '?_sId=' + this._sId : ''));
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>New Question</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form autoComplete="off">
            <FieldGroup id="questionId" data-key="questionId" type="text" value={this.state.questionId} onChange={this._onInputChange} label="Question ID" placeholder="Question ID" help="唯一标识符；使用英文字母或数字；一般第一个版本为v1，每次更新都会递增。" />
            <FieldGroup id="question" data-key="question" type="text" value={this.state.question} onChange={this._onInputChange} label="Question" placeholder="问题" />
            <FieldGroup id="decription" data-key="decription" type="text" value={this.state.decription} onChange={this._onInputChange} label="Descrition" placeholder="问题描述" />
            <CodeFieldGroup id="questionOpts" label="Opts" value={this.state.opts} onChange={this._onOptsChange} readOnly={false} />
            <CodeFieldGroup id="questionSettings" label="Settings" value={this.state.settings} onChange={this._onSettingsChange} readOnly={false} />
            <CodeFieldGroup id="questionHtmlCode" label="Html" value={this.state.html} onChange={this._onHtmlChange} readOnly={false} mode="xml" />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onSubmit}>Submit</Button>
            <Button bsStyle="link" onClick={this._onClearAll}>Clear All</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
