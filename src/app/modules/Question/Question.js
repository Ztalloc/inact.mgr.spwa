import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { PageHeader, Glyphicon, ButtonToolbar, ButtonGroup, Button, Panel } from 'react-bootstrap/lib';

import { LinkContainer } from 'react-router-bootstrap';
import QuestionData from '../../models/question';
import dateComposer from '../../layouts/components/dateComposer';

export default class Question extends React.Component {

  static propTypes = {
    children: PropTypes.node,
    location: PropTypes.object
  }

  constructor() {
    super();
    this.state = {
      items: []
    };
    this._sId = null;
    this._refresh = this._refresh.bind(this);
    this.onRemoveFilter = this.onRemoveFilter.bind(this);
  }

  componentWillMount() {
    if (this.props.location.query._sId) {
      this._sId = this.props.location.query._sId;
      QuestionData.slist(this._sId, (json) => {
        if (json.err.code === 0) {
          this.setState({items: json.data});
        }
      });
    } else {
      this._sId = null;
      QuestionData.get((json) => {
        this.setState({items: json.data});
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.query._sId && nextProps.location.query._sId !== this._sId) {
      this._sId = nextProps.location.query._sId;
      QuestionData.slist(this._sId, (json) => {
        if (json.err.code === 0) {
          this.setState({items: json.data});
        }
      });
    }
  }

  _refresh() {
    browserHistory.push('/question' + (this._sId ? '?_sId=' + this._sId : ''));
    if (this._sId) {
      QuestionData.slist(this._sId, (json) => {
        if (json.err.code === 0) {
          this.setState({items: json.data});
        }
      });
    } else {
      QuestionData.get((json) => {
        this.setState({items: json.data});
      });
    }
  }

  onRemoveFilter() {
    this._sId = null;
    QuestionData.get((json) => {
      this.setState({items: json.data});
      browserHistory.push('/question');
    });
  }

  render() {
    return (
      <div className="container">
        <PageHeader>Question</PageHeader>
        <ButtonToolbar style={{marginBottom: '10px'}}>
          <LinkContainer to={'/question/new' + (this._sId ? '?_sId=' + this._sId : '')}>
            <Button bsStyle="primary">新建问题</Button>
          </LinkContainer>
          { this._sId &&
            <Button onClick={this.onRemoveFilter}>Remove Filter</Button>
          }
          <Button bsStyle="link" className="pull-right" onClick={this._refresh}>刷新列表</Button>
        </ButtonToolbar>
        <div className="row">
        {
          this.state.items.length !== 0 ? this.state.items.map((item) => (
            <div className="col-md-4 col-sm-6" key={item._id}>
              <Panel
                header={item.question ? item.question : '<no question>'}
                footer={
                  <div className="clearfix">
                    <ButtonGroup className="pull-right">
                      <LinkContainer to={'/question/view/' + item._id + (this._sId ? '?_sId=' + this._sId : '')}>
                        <Button>详情</Button>
                      </LinkContainer>
                      <LinkContainer to={'/question/update/' + item._id + (this._sId ? '?_sId=' + this._sId : '')}>
                        <Button>编辑</Button>
                      </LinkContainer>
                      <LinkContainer to={'/question/result/' + item.questionId + (this._sId ? '?_sId=' + this._sId : '')}>
                        <Button>结果</Button>
                      </LinkContainer>
                    </ButtonGroup>
                  </div>
                }
              >
                <p>{item.description ? item.description : '<no description>'}</p>
                <ul className="list-unstyled">
                  <li><Glyphicon glyph="asterisk" /> {item.questionId}</li>
                  <li><Glyphicon glyph="time" /> {dateComposer(item.lastModified)}</li>
                </ul>
              </Panel>
            </div>
          )) :
          <div className="col-md-12">Nothing yet...</div>
        }
        </div>
        {
          this.props.children && React.cloneElement(this.props.children, {
            refresh: this._refresh
          })
        }
      </div>
    );
  }
}
