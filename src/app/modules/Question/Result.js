import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import QuestionData from '../../models/question';

export default class View extends React.Component {

  static propTypes = {
    params: PropTypes.object,
    location: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.questionId = this.props.params.questionId;
    this.state = {
      result: 'Loading...'
    };
    this._sId = null;
    this._getResult = this._getResult.bind(this);
    this._onClose = this._onClose.bind(this);
  }

  componentWillMount() {
    if (this.props.location.query._sId) {
      this._sId = this.props.location.query._sId;
    }
    this._getResult(this.questionId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.questionId !== this.questionId) {
      this.selected_id = nextProps.params.questionId;
      this._getResult(this.questionId);
    }
  }

  _getResult(questionId) {
    QuestionData.result(questionId, (json) => {
      const Result = JSON.stringify(json.data, null, 2);
      this.setState({result: Result});
    });
  }

  _onClose() {
    browserHistory.push('/question' + (this._sId ? '?_sId=' + this._sId : ''));
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose}>
        <Modal.Header closeButton={true}>
          <Modal.Title>Result</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <CodeFieldGroup id="questionResult" label="Result" value={this.state.result} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
