import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import QuestionData from '../../models/question';

export default class Update extends React.Component {

  static propTypes = {
    params: PropTypes.object,
    refresh: PropTypes.func,
    location: PropTypes.object
  }

  constructor(props) {
    super(props);
    this._selected_id = this.props.params._id;
    this._questionOrig = null;
    this.state = {
      questionId: '',
      question: '',
      description: '',
      opts: '',
      html: ''
    };
    this._sId = null;
    this._getOneQuestion = this._getOneQuestion.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._onOptsChange = this._onOptsChange.bind(this);
    this._onSettingsChange = this._onSettingsChange.bind(this);
    this._onHtmlChange = this._onHtmlChange.bind(this);
    this._onUpdate = this._onUpdate.bind(this);
    this._onResume = this._onResume.bind(this);
    this._onClose = this._onClose.bind(this);
  }

  componentWillMount() {
    if (this.props.location.query._sId) {
      this._sId = this.props.location.query._sId;
    }
    this._getOneQuestion(this._selected_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params._id !== this._selected_id) {
      this._selected_id = nextProps.params._id;
      this._getOneQuestion(this._selected_id);
    }
  }

  _getOneQuestion(_id) {
    QuestionData.getOne(_id, (json) => {
      const Question = json.data;
      Question.opts = typeof Question.opts === 'object' ? JSON.stringify(Question.opts, null, 2) : '';
      if (Question.settings) {
        Question.settings = JSON.stringify(Question.settings, null, 2);
      } else {
        Question.settings = '';
      }
      this.setState(Question);
      this._questionOrig = JSON.parse(JSON.stringify(Question));
    });
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onOptsChange(text) {
    this.setState({opts: text});
  }

  _onSettingsChange(text) {
    this.setState({settings: text});
  }

  _onHtmlChange(text) {
    this.setState({html: text});
  }

  _onUpdate() {
    const data = Object.assign({}, this.state);
    delete data._id;
    delete data.created;
    delete data.lastModified;
    delete data.creatorId;
    delete data.status;
    // string
    if (data.questionId === this._questionOrig.questionId) {
      delete data.questionId;
    }
    // string
    if (data.question === this._questionOrig.question) {
      delete data.question;
    }
    // string
    if (data.description === this._questionOrig.description) {
      delete data.description;
    }
    // array
    if (data.opts === this._questionOrig.opts) {
      delete data.opts;
    }
    // object
    if (data.settings === this._questionOrig.settings) {
      delete data.settings;
    }
    // Need to trans obj to str
    if (data.html === this._questionOrig.html) {
      delete data.html;
    }
    if (Object.keys(data).length !== 0) {
      if (data.opts) {
        data.opts = JSON.parse(data.opts);
      }
      if (data.settings) {
        data.settings = JSON.parse(data.settings);
      }
      QuestionData.update(this._selected_id, data, (json) => {
        if (json.err.code === 0) {
          this.props.refresh();
        }
      });
    }
  }

  _onResume() {
    this._getOneQuestion(this._selected_id);
  }

  _onClose() {
    browserHistory.push('/question' + (this._sId ? '?_sId=' + this._sId : ''));
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>Update Question</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FieldGroup id="questionId" data-key="questionId" type="text" value={this.state.questionId} onChange={this._onInputChange} label="Question ID" placeholder="Question ID" help="唯一标识符；使用英文字母或数字或者 _。" />
            <FieldGroup id="question" data-key="question" type="text" value={this.state.question} onChange={this._onInputChange} label="Question" placeholder="问题" />
            <FieldGroup id="description" data-key="description" type="text" value={this.state.description} onChange={this._onInputChange} label="Description" placeholder="问题的描述" />
            <CodeFieldGroup id="questionOpts" label="Opts" value={this.state.opts} onChange={this._onOptsChange} readOnly={false} />
            <CodeFieldGroup id="questionSettings" label="Settings" value={this.state.settings} onChange={this._onSettingsChange} readOnly={false} />
            <CodeFieldGroup id="questionHtmlCode" label="Html" value={this.state.html} onChange={this._onHtmlChange} readOnly={false} mode="xml" />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onUpdate}>Update</Button>
            <Button bsStyle="link" onClick={this._onResume}>Resume</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
