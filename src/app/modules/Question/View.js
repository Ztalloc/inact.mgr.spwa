import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import HtmlViewer from './../../layouts/components/Form/HtmlViewer';
import QuestionData from '../../models/question';

export default class View extends React.Component {

  static propTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.selected_id = this.props.params._id;
    this.state = {
      questionId: '',
      question: '',
      description: '',
      opts: '',
      html: ''
    };
    this._getOneQuestion = this._getOneQuestion.bind(this);
  }

  componentWillMount() {
    this._getOneQuestion(this.selected_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params._id !== this.selected_id) {
      this.selected_id = nextProps.params._id;
      this._getOneQuestion(this.selected_id);
    }
  }

  _getOneQuestion(_id) {
    QuestionData.getOne(_id, (json) => {
      const Question = json.data;
      Question.opts = JSON.stringify(Question.opts, null, 2);
      if (Question.settings) {
        Question.settings = JSON.stringify(Question.settings, null, 2);
      }
      this.setState(Question);
    });
  }

  _onClose() {
    browserHistory.push('/question');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose}>
        <Modal.Header closeButton={true}>
          <Modal.Title>View Question</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FieldGroup id="questionId" data-key="questionId" type="text" value={this.state.questionId} label="Question ID" placeholder="Question ID" help="唯一标识符；使用英文字母或数字或者 _。" disabled={true} />
            <FieldGroup id="question" data-key="question" type="text" value={this.state.question} label="Question" placeholder="问题" disabled={true} />
            <FieldGroup id="description" data-key="description" type="text" value={this.state.description} label="Description" placeholder="问题的描述" disabled={true} />
            <CodeFieldGroup id="questionOpts" label="Opts" value={this.state.opts} />
            {
              this.state.settings ?
              <CodeFieldGroup id="questionSettings" label="Settings" value={this.state.settings} onChange={this._onSettingsChange} />
              :
              null
            }
            <HtmlViewer html={this.state.html} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
