import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import SurveyData from '../../models/survey';

export default class View extends React.Component {

  static propTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.surveyId = this.props.params.surveyId;
    this.surveyName = this.props.params.surveyName;
    this.state = {
      result: 'Press [Build] to build...',
      asset: {},
    };
    this._onBuild = this._onBuild.bind(this);
    this._onBuildPreview = this._onBuildPreview.bind(this);
    this._onUpload = this._onUpload.bind(this);
    this._onFileChange = this._onFileChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.surveyId !== this.surveyId) {
      this.surveyId = nextProps.params.surveyId;
      this.surveyName = nextProps.params.surveyName;
    }
  }

  _onBuild() {
    SurveyData.build(this.surveyId, (text) => {
      this.setState({result: text});
    });
  }

  _onBuildPreview() {
    SurveyData.buildPreview(this.surveyId, (text) => {
      this.setState({result: text});
    });
  }

  _onUpload() {
    SurveyData.uploadAsset(this.surveyName, {fileName: 'asset_file', file: this.state.asset}, (txtResp) => {
      this.setState({result: 'Upload result: ' + txtResp});
    }, false, true);
  }

  _onFileChange(e) {
    e.preventDefault();
    this.setState({asset: e.target.files[0]});
  }

  _onClose() {
    browserHistory.push('/survey');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose}>
        <Modal.Header closeButton={true}>
          <Modal.Title>Build</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <CodeFieldGroup id="surveyBuild" label="Result" value={this.state.result} mode="xml" />
          </form>
          <form>
            <input
              type="file"
              onChange={this._onFileChange}
            />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onBuild}>Build [live]</Button>
            <Button onClick={this._onBuildPreview}>Build [preview]</Button>
            <Button bsStyle="danger" onClick={this._onUpload}>Upload</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
