import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import HtmlEditor from './../../layouts/components/Form/HtmlEditor';
import SurveyData from '../../models/survey';
import {getUser} from '../Auth/auth';

export default class New extends React.Component {

  static propTypes = {
    refresh: PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      surveyId: '',
      title: '',
      name: '',
      description: '',
      html: '',
      questions: '',
      calc: ''
    };
    this._onInputChange = this._onInputChange.bind(this);
    this._onQuestionIdsChange = this._onQuestionsChange.bind(this);
    this._onCalcChange = this._onCalcChange.bind(this);
    this._onSubmit = this._onSubmit.bind(this);
    this._onClearAll = this._onClearAll.bind(this);
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onQuestionsChange(text) {
    this.setState({questions: text});
  }

  _onCalcChange(text) {
    this.setState({calc: text});
  }

  _onSubmit(e) {
    e.preventDefault();
    const data = Object.assign({}, this.state);
    // string
    data.html = data.html.trim();
    if (data.html === '') {
      delete data.html;
    }
    // array
    data.questions = data.questions.trim();
    if (data.questions === '') {
      delete data.questions;
    } else {
      data.questions = JSON.parse(data.questions);
    }
    // object
    data.calc = data.calc.trim();
    if (data.calc === '') {
      delete data.calc;
    } else {
      data.calc = JSON.parse(data.calc);
    }
    // TODO build user module
    data.creatorId = getUser()._id;
    SurveyData.new(data, (json) => {
      if (json.err.code === 0) {
        this.props.refresh();
      }
    });
  }

  _onClearAll() {
    this.setState({
      surveyId: '',
      title: '',
      name: '',
      description: '',
      html: '',
      questions: '',
      calc: ''
    });
  }

  _onClose() {
    browserHistory.push('/survey');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>New Survey</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FieldGroup id="surveyId" data-key="surveyId" type="text" value={this.state.surveyId} onChange={this._onInputChange} label="Survey ID" placeholder="Survey ID" help="唯一标识符；使用英文字母或数字。" />
            <FieldGroup id="surveyTitle" data-key="title" type="text" value={this.state.title} onChange={this._onInputChange} label="Survey Title" placeholder="问卷标题" help="此问卷的主题；生成问卷后，浏览器标题栏上显示的标题。" />
            <FieldGroup id="surveyName" data-key="name" type="text" value={this.state.name} onChange={this._onInputChange} label="Survey Name" placeholder="问卷名称" help="使用英文字母，数字和下划线，将作为访问的URL" />
            <FieldGroup id="surveyDescription" data-key="description" value={this.state.description} onChange={this._onInputChange} type="text" label="Survey Description" placeholder="问卷的详细描述" />
            <HtmlEditor id="surveyHtml" innerHtml={this.state.html} />
            <CodeFieldGroup id="surveyQuestions" label="Questions IDs" value={this.state.questions} onChange={this._onQuestionsChange} readOnly={false} />
            <CodeFieldGroup id="surveyCalcResult" label="Calc" value={this.state.calc} onChange={this._onCalcChange} readOnly={false} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onSubmit}>Submit</Button>
            <Button bsStyle="link" onClick={this._onClearAll}>Clear All</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
