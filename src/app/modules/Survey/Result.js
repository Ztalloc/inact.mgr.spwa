import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import SurveyData from '../../models/survey';

export default class View extends React.Component {

  static propTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.surveyId = this.props.params.surveyId;
    this.state = {
      result: ''
    };
    this._getResult = this._getResult.bind(this);
  }

  componentWillMount() {
    this._getResult(this.surveyId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.surveyId !== this.surveyId) {
      this.selected_id = nextProps.params.surveyId;
      this._getResult(this.surveyId);
    }
  }

  _getResult(surveyId) {
    SurveyData.result(surveyId, (json) => {
      const Result = JSON.stringify(json.data, null, 2);
      this.setState({result: Result});
    });
  }

  _onClose() {
    browserHistory.push('/survey');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose}>
        <Modal.Header closeButton={true}>
          <Modal.Title>Survey's Result</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <CodeFieldGroup id="surveyCalcResult" label="Result" value={this.state.result} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}