import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { PageHeader, Glyphicon, ButtonToolbar, ButtonGroup, Button, OverlayTrigger, Tooltip } from 'react-bootstrap/lib';
import { LinkContainer } from 'react-router-bootstrap';
import SurveyData from '../../models/survey';
import dateComposer from '../../layouts/components/dateComposer';

export default class Survey extends React.Component {

  static propTypes = {
    children: PropTypes.node
  }

  constructor() {
    super();
    this.state = {
      items: []
    };
    this._refresh = this._refresh.bind(this);
  }

  componentWillMount() {
    SurveyData.get((json) => {
      this.setState({items: json.data});
    });
  }

  _refresh() {
    browserHistory.push('/survey');
    SurveyData.get((json) => {
      this.setState({items: json.data});
    });
  }

  render() {
    const ttSurveyId = (<Tooltip id="tooltipSurveyId">问卷ID</Tooltip>);
    const ttLastModified = (<Tooltip id="tooltipLastModified">修改时间</Tooltip>);
    const ttCreated = (<Tooltip id="tooltipCreated">创建时间</Tooltip>);
    const ttViews = (<Tooltip id="tooltipViews">访问量</Tooltip>);
    return (
      <div className="container">
        <PageHeader>Survey</PageHeader>
        <ButtonToolbar style={{marginBottom: '10px'}}>
          <LinkContainer to="/survey/new">
            <Button bsStyle="primary">新建问卷</Button>
          </LinkContainer>
          <Button bsStyle="link" className="pull-right" onClick={this._refresh}>刷新数据</Button>
        </ButtonToolbar>
        <ul className="list-group">
          {
            this.state.items.length !== 0 ? this.state.items.map(item => (
            <li className="list-group-item" key={item._id}>
              <div className="row">
                <div className="col-md-10">
                  <ul className="list-unstyled">
                    <li>{item.title}</li>
                    <li>{item.name}</li>
                    <li>{item.description}</li>
                    { (item.status & 0x2) ? <li><span className="label label-success">已发布</span></li> : null }
                    <li><OverlayTrigger placement="left" overlay={ttSurveyId}><Glyphicon glyph="asterisk" /></OverlayTrigger> {item.surveyId}</li>
                    <li><OverlayTrigger placement="left" overlay={ttLastModified}><Glyphicon glyph="time" /></OverlayTrigger> {dateComposer(item.lastModified)}</li>
                    <li><OverlayTrigger placement="left" overlay={ttCreated}><Glyphicon glyph="calendar" /></OverlayTrigger> {dateComposer(item.created)}</li>
                    <li><OverlayTrigger placement="left" overlay={ttViews}><Glyphicon glyph="eye-open" /></OverlayTrigger> {item.statistics.visited}</li>
                  </ul>
                </div>
                <div className="col-md-2">
                  <ButtonGroup vertical={true} block={true}>
                    <LinkContainer to={'/survey/view/' + item._id}>
                      <Button block={true}>详情</Button>
                    </LinkContainer>
                    <LinkContainer to={'/survey/update/' + item._id}>
                      <Button block={true}>编辑</Button>
                    </LinkContainer>
                    <LinkContainer to={'/question?_sId=' + item._id}>
                      <Button block={true}>问题列表</Button>
                    </LinkContainer>
                    <LinkContainer to={'/survey/result/' + item.surveyId}>
                      <Button block={true}>结果</Button>
                    </LinkContainer>
                    <LinkContainer to={'/survey/build/' + item.surveyId + '/' + item.name}>
                      <Button block={true}>生成</Button>
                    </LinkContainer>
                  </ButtonGroup>
                </div>
              </div>
            </li>
            ))
            :
            <li className="list-group-item">Nothing yet...</li>
          }
        </ul>
        {
          this.props.children && React.cloneElement(this.props.children, {
            refresh: this._refresh
          })
        }
      </div>
    );
  }
}
