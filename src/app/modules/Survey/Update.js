import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import HtmlEditor from './../../layouts/components/Form/HtmlEditor';
import SurveyData from '../../models/survey';

export default class Update extends React.Component {

  static propTypes = {
    params: PropTypes.object,
    refresh: PropTypes.func
  }

  constructor(props) {
    super(props);
    this._selected_id = this.props.params._id;
    this._surveyOrig = null;
    this.html = null;
    this.state = {
      surveyId: '',
      title: '',
      name: '',
      description: '',
      status: '',
      questions: '',
      settings: '',
      calc: '',
      html: ''
    };
    this._getOneSurvey = this._getOneSurvey.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._onQuestionsChange = this._onQuestionsChange.bind(this);
    this._onSettingsChange = this._onSettingsChange.bind(this);
    this._onCalcChange = this._onCalcChange.bind(this);
    this._onUpdate = this._onUpdate.bind(this);
    this._onResume = this._onResume.bind(this);
  }

  componentWillMount() {
    this._getOneSurvey(this._selected_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params._id !== this._selected_id) {
      this._selected_id = nextProps.params._id;
      this._getOneSurvey(this._selected_id);
    }
  }

  _getOneSurvey(_id) {
    SurveyData.getOne(_id, (json) => {
      const Survey = json.data;
      Survey.status = '0x' + Survey.status.toString(16).toUpperCase();
      Survey.calc = JSON.stringify(Survey.calc, null, 2);
      Survey.settings = JSON.stringify(Survey.settings, null, 2);
      Survey.questions = JSON.stringify(Survey.questions, null, 2);
      this.setState(Survey);
      this._surveyOrig = JSON.parse(JSON.stringify(Survey));
    });
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onQuestionsChange(text) {
    this.setState({questions: text});
  }

  _onSettingsChange(text) {
    this.setState({settings: text});
  }

  _onCalcChange(text) {
    this.setState({calc: text});
  }

  _onUpdate(e) {
    e.preventDefault();
    const data = Object.assign({}, this.state);
    data.html = this.html;
    delete data._id;
    delete data.created;
    delete data.lastModified;
    delete data.creatorId;
    // string
    if (data.surveyId === this._surveyOrig.surveyId) {
      delete data.surveyId;
    }
    // string
    if (data.title === this._surveyOrig.title) {
      delete data.title;
    }
    // string
    if (data.name === this._surveyOrig.name) {
      delete data.name;
    }
    // string
    if (data.description === this._surveyOrig.description) {
      delete data.description;
    }
    // number
    if (data.status === this._surveyOrig.status) {
      delete data.status;
    } else {
      data.status = parseInt(data.status, 16);
    }
    // string
    if (data.html === this._surveyOrig.html) {
      delete data.html;
    }
    // object
    if (data.questions === this._surveyOrig.questions) {
      delete data.questions;
    } else {
      data.questions = JSON.parse(data.questions);
    }
    // object
    if (data.settings === this._surveyOrig.settings) {
      delete data.settings;
    } else {
      data.settings = JSON.parse(data.settings);
    }
    // object (with keys) {result, testify}
    if (data.calc === this._surveyOrig.calc) {
      delete data.calc;
    } else {
      data.calc = JSON.parse(data.calc);
    }
    console.log(data);
    if (Object.keys(data).length !== 0) {
      SurveyData.update(this._selected_id, data, (json) => {
        if (json.err.code === 0) {
          this.props.refresh();
        }
      });
    } else {
      alert('No Update');
    }
  }

  _onResume() {
    this._getOneSurvey(this._selected_id);
  }

  _onClose() {
    browserHistory.push('/survey');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>Update Survey</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FieldGroup id="surveyId" data-key="surveyId" type="text" onChange={this._onInputChange} label="Survey ID" placeholder="Survey ID" help="唯一标识符；使用英文字母或数字；一般第一个版本为v1，每次更新都会递增。" value={this.state.surveyId} />
            <FieldGroup id="surveyTitle" data-key="title" type="text" onChange={this._onInputChange} label="Survey Title" placeholder="问卷标题" help="此问卷的主题；生成问卷后，浏览器标题栏上显示的标题。" value={this.state.title} />
            <FieldGroup id="surveyName" data-key="name" type="text" onChange={this._onInputChange} label="Survey Name" placeholder="问卷名称" help="使用英文字母，数字和下划线，将作为访问的URL" value={this.state.name} />
            <FieldGroup id="surveyDescription" data-key="description" type="text" onChange={this._onInputChange} label="Survey Description" placeholder="问题的详细描述" value={this.state.description} />
            <FieldGroup id="surveyStatus" data-key="status" type="text" onChange={this._onInputChange} label="Survey Status" placeholder="Status" value={this.state.status} />
            <CodeFieldGroup id="surveyQuestions" onChange={this._onQuestionsChange} label="Questions IDs" help="构成这个问卷的问题的ID。" value={this.state.questions} readOnly={false} />
            <CodeFieldGroup id="surveySettings" onChange={this._onSettingsChange} label="Settings" value={this.state.settings} readOnly={false} />
            <CodeFieldGroup id="surveyCalc" onChange={this._onCalcChange} label="Calc Methods" placeholder="最终结果的计算方式" help="一个对象：initialVal, arrayParam, parseInt。JSON.stringify()" value={this.state.calc} readOnly={false} />
            <HtmlEditor id="surveyHtml" innerHtml={this.state.html} getHtml={(html) => {this.html = html}} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onUpdate}>Update</Button>
            <Button bsStyle="link" onClick={this._onResume}>Resume</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
