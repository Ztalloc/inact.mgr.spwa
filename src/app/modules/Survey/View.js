import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import SurveyData from '../../models/survey';

export default class View extends React.Component {

  static propTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.selected_id = this.props.params._id;
    this.state = {
      surveyId: '',
      title: '',
      name: '',
      description: '',
      createDate: '',
      calc: '',
      questions: '',
      html: ''
    };
    this._getOneSurvey = this._getOneSurvey.bind(this);
  }

  componentWillMount() {
    this._getOneSurvey(this.selected_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params._id !== this.selected_id) {
      this.selected_id = nextProps.params._id;
      this._getOneSurvey(this.selected_id);
    }
  }

  _getOneSurvey(_id) {
    SurveyData.getOne(_id, (json) => {
      const Survey = json.data;
      if (Survey.calc) {
        Survey.calc = JSON.stringify(Survey.calc, null, 2);
      }
      Survey.questions = Survey.questions === '' ? '' : JSON.stringify(Survey.questions, null, 2);
      Survey.html = Survey.html === '' ? '' : JSON.stringify(Survey.html);
      this.setState(Survey);
    });
  }

  _onClose() {
    browserHistory.push('/survey');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose}>
        <Modal.Header closeButton={true}>
          <Modal.Title>View Survey</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FieldGroup id="surveyId" type="text" label="Survey ID" placeholder="Survey ID" help="唯一标识符；使用英文字母或数字；一般第一个版本为v1，每次更新都会递增" value={this.state.surveyId} disabled={true} />
            <FieldGroup id="surveyTitle" type="text" label="Survey Title" placeholder="问卷标题" help="此问卷的主题；生成问卷后，浏览器标题栏上显示的标题" value={this.state.title} disabled={true} />
            <FieldGroup id="surveyName" type="text" label="Survey Name" placeholder="问卷名称" help="使用英文字母，数字和下划线，将作为访问的URL" value={this.state.name} disabled={true} />
            <FieldGroup id="surveyDescription" type="text" label="Survey Description" placeholder="问卷的详细描述" value={this.state.description} disabled={true} />
            <FieldGroup id="surveyHtml" type="text" label="Survey Cover HTML" placeholder="问卷首页的HTML结构代码" help="生成问卷后，问卷的第一页内容。JSON.stringify()" value={this.state.html} disabled={true} />
            <CodeFieldGroup id="surveyQuestions" label="Questions IDs" help="构成这个问卷的问题的ID。" value={this.state.questions} />
            <CodeFieldGroup id="surveyCalcResult" label="Calc Methods" help="一个对象：initialVal, arrayParam, parseInt。JSON.stringify()" value={this.state.calc} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
