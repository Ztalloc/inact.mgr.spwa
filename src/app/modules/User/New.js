import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Alert, Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import UserData from '../../models/user';

export default class New extends React.Component {

  static propTypes = {
    refresh: PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      lastName: '',
      firstName: '',
      nickName: '',
      email: '',
      pass: '',
      passConfirm: '',
      roleID: '0x100',
      levelID: '0x100',
      alert: false,
      msg: ''
    };
    this._onInputChange = this._onInputChange.bind(this);
    this._onSubmit = this._onSubmit.bind(this);
    this._onClearAll = this._onClearAll.bind(this);
    this._onAlertDismiss = this._onAlertDismiss.bind(this);
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onSubmit(e) {
    e.preventDefault();
    const data = Object.assign({}, this.state);
    delete data.alert;
    delete data.msg;
    // string
    data.lastName = data.lastName.trim();
    if (data.lastName === '') {
      this.setState({msg: 'lastName is empty!', alert: true});
      return;
    }
    // string
    data.firstName = data.firstName.trim();
    if (data.firstName === '') {
      this.setState({msg: 'firstName is empty!', alert: true});
      return;
    }
    // string
    data.nickName = data.nickName.trim();
    if (data.nickName === '') {
      this.setState({msg: 'nickName is empty!', alert: true});
      return;
    }
    // string
    data.email = data.email.trim();
    if (data.email === '') {
      this.setState({msg: 'email is empty!', alert: true});
      return;
    }
    // string
    if (data.pass === '') {
      this.setState({msg: 'pass is empty!', alert: true});
      return;
    }
    if (data.pass !== data.passConfirm) {
      this.setState({msg: 'Passwords are not unify!', alert: true, pass: '', passConfirm: ''});
      return;
    }
    data.auth = {pass: data.pass};
    data.privilege = {
      roleID: data.roleID,
      levelID: data.levelID
    }
    delete data.pass;
    delete data.roleID;
    delete data.levelID;
    delete data.passConfirm;
    UserData.new(data, (json) => {
      if (json.err.code === 0) {
        this.props.refresh();
      }
    });
  }

  _onClearAll() {
    this.setState({
      lastName: '',
      firstName: '',
      nickName: '',
      email: '',
      pass: '',
      roleID: '0x100',
      levelID: '0x100'
    });
  }

  _onAlertDismiss() {
    this.setState({alert: false});
  }

  _onClose() {
    browserHistory.push('/user');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>New User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            this.state.alert &&
            <Alert bsStyle="danger" onDismiss={this._onAlertDismiss}>
              <p>{this.state.msg}</p>
            </Alert>
          }
          <form>
            <FieldGroup id="lastName" data-key="lastName" type="text" value={this.state.lastName} onChange={this._onInputChange} label="Last Name" placeholder="Last Name" />
            <FieldGroup id="firstName" data-key="firstName" type="text" value={this.state.firstName} onChange={this._onInputChange} label="First Name" placeholder="First Name"/>
            <FieldGroup id="nickName" data-key="nickName" type="text" value={this.state.nickName} onChange={this._onInputChange} label="Nick Name" placeholder="Nick Name" />
            <FieldGroup id="email" data-key="email" type="text" value={this.state.email} onChange={this._onInputChange} label="Email" placeholder="Email" />
            <FieldGroup id="pass" data-key="pass" type="password" value={this.state.pass} onChange={this._onInputChange} label="Password" placeholder="Password" />
            <FieldGroup id="passConfirm" data-key="passConfirm" type="password" value={this.state.pass} onChange={this._onInputChange} label="Confirm Password" placeholder="Confirm Password" />
            <FieldGroup id="roleID" data-key="roleID" type="text" value={this.state.roleID} onChange={this._onInputChange} label="roleID" placeholder="roleID" />
            <FieldGroup id="levelID" data-key="levelID" type="text" value={this.state.roleID} onChange={this._onInputChange} label="levelID" placeholder="levelID" />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onSubmit}>Submit</Button>
            <Button bsStyle="link" onClick={this._onClearAll}>Clear All</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
