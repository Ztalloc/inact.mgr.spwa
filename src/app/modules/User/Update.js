import React, {PropTypes} from 'react';
import { browserHistory } from 'react-router';
import { Alert, Modal, ButtonToolbar, Button } from 'react-bootstrap/lib';

import FieldGroup from './../../layouts/components/Form/FieldGroup';
import CodeFieldGroup from './../../layouts/components/Form/CodeFieldGroup';
import UserData from '../../models/user';

export default class Update extends React.Component {

  static propTypes = {
    refresh: PropTypes.func,
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      lastName: '',
      firstName: '',
      nickName: '',
      email: '',
      mobilePhone: '',
      pass: '',
      passConfirm: '',
      privilege: '',
      status: '',
      alert: false,
      msg: ''
    };
    this._selected_id = this.props.params._id;
    this._userOrig = this.state;
    this._getOneUser = this._getOneUser.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._onPrivilegeChange = this._onPrivilegeChange.bind(this);
    this._onStatusChange = this._onStatusChange.bind(this);
    this._onUpdate = this._onUpdate.bind(this);
    this._onResume = this._onResume.bind(this);
    this._onAlertDismiss = this._onAlertDismiss.bind(this);
  }

  componentWillMount() {
    this._getOneUser(this._selected_id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params._id !== this._selected_id) {
      this._selected_id = nextProps.params._id;
      this._getOneUser(this._selected_id);
    }
  }

  _getOneUser(_id) {
    UserData.getOne(_id, (json) => {
      const User = Object.assign(this.state, json.data);
      User.privilege = JSON.stringify(User.privilege, null, 2);
      User.status = JSON.stringify(User.status);
      this.setState(User);
      this._userOrig = User;
    });
  }

  _onInputChange(e) {
    this.setState({[e.target.getAttribute('data-key')]: e.target.value});
  }

  _onPrivilegeChange(text) {
    this.setState({privilege: text});
  }

  _onStatusChange(text) {
    this.setState({status: text});
  }

  _onUpdate(e) {
    e.preventDefault();
    const data = Object.assign({}, this.state);
    delete data._id;
    delete data.alert;
    delete data.msg;
    // string
    data.lastName = data.lastName.trim();
    if (data.lastName === this._userOrig.lastName) {
      delete data.lastName;
    }
    // string
    data.firstName = data.firstName.trim();
    if (data.firstName === this._userOrig.firstName) {
      delete data.firstName;
    }
    // string
    data.nickName = data.nickName.trim();
    if (data.nickName === this._userOrig.nickName) {
      delete data.nickName;
    }
    // string
    data.email = data.email.trim();
    if (data.email === this._userOrig.email) {
      delete data.email;
    }
    // string
    data.mobilePhone = data.mobilePhone.trim();
    if (data.mobilePhone === this._userOrig.mobilePhone) {
      delete data.mobilePhone;
    }
    // string
    if (data.pass === '') {
      delete data.pass;
    } else {
      if (data.pass !== data.passConfirm) {
        this.setState({msg: 'Passwords are not unify!', alert: true, pass: '', passConfirm: ''});
        return;
      }
    }
    // object
    if (data.privilege === this._userOrig.privilege) {
      delete data.privilege;
    } else {
      data.privilege = JSON.parse(data.privilege);
    }
    // object
    if (data.status === this._userOrig.status) {
      delete data.status;
    } else {
      data.status = JSON.parse(data.status);
    }
    delete data.passConfirm;
    const body = {
      update: {
        method: '$set',
        data: data
      }
    };
    if (Object.keys(body.update.data).length !== 0) {
      UserData.update(this._selected_id, body, (json) => {
        if (json.err.code === 0) {
          this.props.refresh();
        }
      });
    } else {
      browserHistory.push('/user');
    }
  }

  _onResume() {
    this.setState(this._userOrig);
  }

  _onAlertDismiss() {
    this.setState({alert: false});
  }

  _onClose() {
    browserHistory.push('/user');
  }

  render() {
    return (
      <Modal bsSize="lg" show={true} onHide={this._onClose} backdrop="static">
        <Modal.Header closeButton={true}>
          <Modal.Title>Update User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            this.state.alert &&
            <Alert bsStyle="danger" onDismiss={this._onAlertDismiss}>
              <p>{this.state.msg}</p>
            </Alert>
          }
          <form>
            <FieldGroup id="lastName" data-key="lastName" type="text" value={this.state.lastName} onChange={this._onInputChange} label="Last Name" placeholder="Last Name" />
            <FieldGroup id="firstName" data-key="firstName" type="text" value={this.state.firstName} onChange={this._onInputChange} label="First Name" placeholder="First Name"/>
            <FieldGroup id="nickName" data-key="nickName" type="text" value={this.state.nickName} onChange={this._onInputChange} label="Nick Name" placeholder="Nick Name" />
            <FieldGroup id="email" data-key="email" type="text" value={this.state.email} onChange={this._onInputChange} label="Email" placeholder="Email" />
            <FieldGroup id="mobilePhone" data-key="mobilePhone" type="text" value={this.state.mobilePhone} onChange={this._onInputChange} label="Mobile Phone" placeholder="Phone number" />
            <FieldGroup id="pass" data-key="pass" type="password" value={this.state.pass} onChange={this._onInputChange} label="Password" placeholder="Password" />
            <FieldGroup id="passConfirm" data-key="passConfirm" type="password" value={this.state.pass} onChange={this._onInputChange} label="Confirm Password" placeholder="Confirm Password" />
            <CodeFieldGroup id="userPrivilege" label="Privilege" value={this.state.privilege} onChange={this._onPrivilegeChange} readOnly={false} />
            <CodeFieldGroup id="userStatus" label="Status" value={this.state.status} onChange={this._onStatusChange} readOnly={false} />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="primary" onClick={this._onUpdate}>Update</Button>
            <Button bsStyle="link" onClick={this._onResume}>Resume</Button>
            <Button onClick={this._onClose} className="pull-right">Close</Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}
