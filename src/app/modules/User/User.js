import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { PageHeader, ButtonToolbar, Button, Glyphicon, ButtonGroup } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import UserData from '../../models/user';
import dateComposer from '../../layouts/components/dateComposer';

export default class User extends React.Component {

  static propTypes = {
    children: PropTypes.node
  }

  constructor() {
    super();
    this.state = {
      users: []
    };
    this._userGet = this._userGet.bind(this);
    this._refresh = this._refresh.bind(this);
  }

  componentWillMount() {
    this._userGet();
  }

  _userGet() {
    UserData.get((json) => {
      this.setState({users: json.data});
    });
  }

  _refresh() {
    browserHistory.push('/user');
    this._userGet();
  }

  render() {
    return (
      <div className="container">
        <PageHeader>User</PageHeader>
        <ButtonToolbar style={{marginBottom: '10px'}}>
          <LinkContainer to="/user/new">
            <Button bsStyle="primary">New User</Button>
          </LinkContainer>
          <Button bsStyle="link" className="pull-right" onClick={this._refresh}>Refresh</Button>
        </ButtonToolbar>
        <ul className="list-group">
          {
            this.state.users.length !== 0 ? this.state.users.map(user => (
              <li className="list-group-item" key={user._id}>
                <div className="row">
                  <div className="col-md-10">
                    <h4>{user.nickName}</h4>
                    <ul className="list-unstyled">
                      <li><Glyphicon glyph="asterisk" /> {user._id}</li>
                      <li><Glyphicon glyph="envelope" /> {user.email}</li>
                      <li><Glyphicon glyph="phone" /> {user.mobilePhone}</li>
                      <li><Glyphicon glyph="time" /> {dateComposer(user.created)}</li>
                    </ul>
                  </div>
                  <div className="col-md-2">
                    <ButtonGroup vertical={true} block={true}>
                      <LinkContainer to={'/user/update/' + user._id}>
                        <Button block={true}>编辑</Button>
                      </LinkContainer>
                    </ButtonGroup>
                  </div>
                </div>
              </li>
            ))
            :
            <li className="list-group-item">Nothing yet...</li>
          }
        </ul>
        {
          this.props.children && React.cloneElement(this.props.children, {
            refresh: this._refresh
          })
        }
      </div>
    );
  }
}
