/* eslint no-undef: "warn" */
import App from '../App';

/**
 * Show error if something went wrong during chunk loading
 */
const errorLoading = err => console.error('Dynamic page loading failed', err);

/**
 * Parse loaded module
 */
const loadRoute = cb => {
  return module => {
    cb(null, module.default);
  };
};

const routes = {
  path: '/',
  component: App,
  indexRoute: {
    getComponent(nextState, cb) {
      System.import('../modules/Home')
            .then(loadRoute(cb))
            .catch(errorLoading);
    }
  },
  childRoutes: [
    {
      path: '/user',
      getComponent(nextState, cb) {
        System.import('../modules/User/User')
              .then(loadRoute(cb))
              .catch(errorLoading);
      },
      childRoutes: [
        {
          path: '/user/new',
          getComponent(nextState, cb) {
            System.import('../modules/User/New')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/user/update/:_id',
          getComponent(nextState, cb) {
            System.import('../modules/User/Update')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        }
      ]
    },
    {
      path: '/question',
      getComponent(nextState, cb) {
        System.import('../modules/Question/Question')
              .then(loadRoute(cb))
              .catch(errorLoading);
      },
      childRoutes: [
        {
          path: '/question/new',
          getComponent(nextState, cb) {
            System.import('../modules/Question/New')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/question/view/:_id',
          getComponent(nextState, cb) {
            System.import('../modules/Question/View')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/question/update/:_id',
          getComponent(nextState, cb) {
            System.import('../modules/Question/Update')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/question/result/:questionId',
          getComponent(nextState, cb) {
            System.import('../modules/Question/Result')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        }
      ]
    },
    {
      path: '/survey',
      getComponent(nextState, cb) {
        System.import('../modules/Survey/Survey')
              .then(loadRoute(cb))
              .catch(errorLoading);
      },
      childRoutes: [
        {
          path: '/survey/new',
          getComponent(nextState, cb) {
            System.import('../modules/Survey/New')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/survey/view/:_id',
          getComponent(nextState, cb) {
            System.import('../modules/Survey/View')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/survey/update/:_id',
          getComponent(nextState, cb) {
            System.import('../modules/Survey/Update')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/survey/result/:surveyId',
          getComponent(nextState, cb) {
            System.import('../modules/Survey/Result')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        },
        {
          path: '/survey/build/:surveyId/:surveyName',
          getComponent(nextState, cb) {
            System.import('../modules/Survey/Build')
                  .then(loadRoute(cb))
                  .catch(errorLoading);
          }
        }
      ]
    },
    {
      path: '/asset',
      getComponent(nextState, cb) {
        System.import('../modules/Asset/Asset')
              .then(loadRoute(cb))
              .catch(errorLoading);
      }
    },
    {
      path: '/logout',
      getComponent(location, cb) {
        System.import('../modules/Auth/Logout')
              .then(loadRoute(cb))
              .catch(errorLoading);
      }
    }
  ]
};

export default routes;
