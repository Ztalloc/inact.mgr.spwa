import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';

import { update as authUpdate } from './app/modules/Auth/auth';
import routes from './app/routes';
import './app/css/index.css';
import 'codemirror/mode/javascript/javascript';
require('codemirror/mode/xml/xml');
require('codemirror/lib/codemirror.css');
browserHistory.listen(() => {
  authUpdate();
});

ReactDOM.render(
  <Router history={browserHistory} routes={routes} />,
  document.getElementById('root')
);
